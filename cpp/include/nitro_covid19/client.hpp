#ifndef SRC_CLIENT_HPP_
#define SRC_CLIENT_HPP_

#include <iostream>
#include <string>
#include <zmq.hpp>

#include "client_cli.hpp"
#include "client_msg_mgr.hpp"


#define IPC_PATH "ipc:///tmp/covid_channel"


/*
 * Corona virus client class.
 *
 * Responsible to handle client front-end, get commands from him
 * and verify them(using client_cli), build matching protobuf messages
 * and handle communicate with server(including parsing server's replies).
 */
class client_c
{
public:
	client_c(void);
	void run(void);
	virtual ~client_c(void);

private:
	client_cli_c m_cli;
	zmq::context_t *m_ctx;
	zmq::socket_t *m_sock;
	std::string m_country;
	bool send(const std::string &cmd);
	bool poll_recv(zmq::pollitem_t items[], std::string *reply_str);
	bool handle_cmd(
			std::string cmd_str, zmq::pollitem_t items [], bool is_start_session=false);
};


#endif /* SRC_CLIENT_HPP_ */
