#ifndef SERVER_HPP_
#define SERVER_HPP_

#include <string>
#include <iostream>
#include <zmq.hpp>
#ifndef _WIN32
#include <unistd.h>
#else
#include <windows.h>
#endif

#include "server_msg_mgr.hpp"


#define IPC_PATH "ipc:///tmp/covid_channel"
#define DB_FILE_NAME "covid19_db.txt"


/*
 * Corona virus server class.
 *
 * Responsible for managing client sessions, communication from/to client.
 * Handle client commands and reads & writes to DB.
 */
class server_c
{
public:
	server_c(void);
	void run(void);
	virtual ~server_c(void);

private:
	const std::string db_file_name;
	zmq::context_t *m_ctx;
	zmq::socket_t *m_sock;
	std::vector<std::string> m_active_sessions;
	bool send(const std::string &msg_str);
	std::string recv(void);
	bool parse_msg(std::string *msg, server_msg_builder_c **msg_builder);
};


#endif /* SERVER_HPP_ */
