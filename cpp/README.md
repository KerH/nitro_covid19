# C++ Training

This repository will help you work on your `C++` skills and learn `Protobuf`.

## Exercise Goals
By the time you complete this exercise you will know:
1. How to write and compile `C++` code.
2. Using the `clang-tidy` cpp linter in your CI.
3. How to seriallize message using the `Protobuf` mechanism.
4. `0MQ` high level API.

In this exercise we are joining the fight of the world against the [Coronavirus
pandemic](https://en.wikipedia.org/wiki/2019%E2%80%9320_coronavirus_pandemic). 

**We will do this by writing covid19 client and server code!**


One of the most important parts in the fight is being updated with the number of
**deaths** and **new cases** in the world and of course letting the world the option
to update their own numbers as well.

## Server
Your server will be responsible for saving and managing the world `covid19`
information. The clients for your server will be the different countries 
in the world looking to update the different number on their own coutry and 
getting the most updated numbers from the rest of the world.

1. The server will communicate with the clients using `0MQ` ipc. `0MQ` have
a very good [documentation](https://zeromq.org/get-started/), so make sure you are taking a look. 
2. The server will know to handle the next messages from the clients:
    * **start_session** - every session of a client start with such message.
    Using the message the client authorizing as specific country and starts a session. You can assume each country has a single client.
    * **update_cases** - update the **new** number of deaths and new number of cases.
    * **get_cases** - retrieve the number of cases of given country (can be all countries if no country is given)
    * **end_session** - end the current session and allows other client from the country to start session.
3. The messages will be encoded and decoded using `Protobuf` before sent 
over the ipc transport (go to the protofbuf part now :) i expect to see a .proto file in your repo).
4. Your Server will need to have a database for all the information,
you can use whatever format you want for this (csv, json, etc.).
5. I expect to see a server class that is not bound to any specific async library called `covid19_server_c`.
Remember to supply an API that can be used by any specific implementation of your base server.
6. Supply implementation to your base server and poll for events somehow.

   **BONUS**: use `libuv` for event polling.
   
7. Since you already did the `CMake` training you obviously will choose to build
your project with `CMake`. When building your project is expect to have two separate binaries: covid19_server, covid19_client.


## Client
Your client will be responsible for sending the described messages to the server
on top of the `0MQ` ipc transport. 

1. The client need to support sending all the messages described above,
and presenting their answer nicely. It can be using a CLI or interactivly with
the user.
2. Point 6 in the server.

**BONUS**: present the number of cases in a nice table/graph.


## General
1. I expect to see a well build CI checking your code.
2. Your projects structure should be like described [here](https://medium.com/heuristics/c-application-development-part-1-project-structure-454b00f9eddc)


### I wish you good luck joining the fight, may the force be with you

```
Art by Joan G. Stark
       .---.
  ___ /_____\
 /\.-`( '.' )
/ /    \_-_/_
\ `-.-"`'V'//-.
 `.__,   |// , \
     |Ll //Ll|\ \
     |__//   | \_\
    /---|[]==| / /
    \__/ |   \/\/
    /_   | Ll_\|
     |`^"""^`|
     |   |   |
     |   |   |
     |   |   |
     |   |   |
     L___l___J
 jgs  |_ | _|
     (___|___)
      ^^^ ^^^
```
