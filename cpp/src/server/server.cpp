#include <nitro_covid19/server.hpp>


#define RECV_TIMEOUT 1000


/*
 * Server constructor.
 *
 * Create new context and socket for inter-process-communication from/to client.
 * Binding socket.
 */
server_c::server_c(void) : db_file_name(DB_FILE_NAME) {
	m_ctx = new zmq::context_t(1);
	m_sock = new zmq::socket_t(*m_ctx, ZMQ_REP);
	// binding the socket
	(*m_sock).bind(IPC_PATH);
}


/*
 * Server destructor.
 *
 * Free memory for zmq socket and context.
 */
server_c::~server_c(void) {
	// delete socket
	delete m_sock;

	// context closed and deleted
	delete m_ctx;
}


/*
 * Send message to client over zmq socket.
 *
 * Parameters:
 *  @msg_str(In) - String serialization of protobuf converted reply message from server.
 *
 * return value - true in case send succeeded, false otherwise.
 */
bool server_c::send(const std::string &msg_str) {
    zmq::message_t msg(msg_str.size());
    std::memcpy (msg.data(), msg_str.data(), msg_str.size());
    bool rc = (*m_sock).send(msg);
    return (rc);
}


/*
 * Receive message from client.
 *
 * return value - C like string with message data.
 */
std::string server_c::recv(void) {
	zmq::message_t request;
	(*m_sock).recv(&request, 0);
	return std::string(static_cast<char*>(request.data()), request.size());
}


/*
 * Parse given message to matching message builder.
 *
 * Parameters:
 *  @msg(In)          - Protobug message serialized as string.
 *  @msg_builder(Out) - Pointer to message builder that will be allocate inside this function.
 *
 * return value - true if protobuf parsing was OK and matching builder was created
 *  			  with parameters, false otherwise.
 */
bool server_c::parse_msg(std::string *msg, server_msg_builder_c **msg_builder) {
	nitro_covid19::ClientCommand cmd;

	if(!cmd.ParseFromString(*msg)) {
		return false;
	}

	// protobuf parsing ok
	if(cmd.message_type() == nitro_covid19::ClientCommand::START_SESSION) {
		*msg_builder = new start_session_builder_c(cmd.country(), &m_active_sessions);
	} else if(cmd.message_type() == nitro_covid19::ClientCommand::UPDATE_CASES) {
		*msg_builder = new update_cases_builder_c(
				cmd.country(), cmd.args(0).sick_people(), cmd.args(0).dead_people(), db_file_name);
	} else if(cmd.message_type() == nitro_covid19::ClientCommand::GET_CASES) {
		*msg_builder = new get_cases_builder_c(cmd.args(0).country(), db_file_name);
	} else if(cmd.message_type() == nitro_covid19::ClientCommand::END_SESSION) {
		*msg_builder = new end_session_builder_c(cmd.country(), &m_active_sessions);
	} else {
		// otherwise that's an error
		return false;
	}
	// parsing completed
	return true;
}


/*
 * Main server function.
 *
 * Responsible to receive messages from clients, handle them,
 * and reply clients with matching message.
 */
void server_c::run(void) {
	std::string in_msg, out_msg;
	server_msg_builder_c *msg_builder;

	std::cout << "Corona virus information server, start running..." << std::endl;

	// initialize poll set
	zmq::pollitem_t items [] = {
		{static_cast<void*>(*m_sock), 0, ZMQ_POLLIN, 0}
	};
	nitro_covid19::ClientCommand cmd;

	// process messages from socket
	while (1) {
		int rc = zmq::poll(items, 1, RECV_TIMEOUT);
		if(rc < 0) {
			std::cout << "Error occured while polling for client command:" +\
					std::to_string(rc) << std::endl;
			return;
		}
		if(items[0].revents & ZMQ_POLLIN) {
			// wait for next request from client
			in_msg = recv();
			if(!parse_msg(&in_msg, &msg_builder) || !msg_builder->build_msg(&out_msg)) {
				nitro_covid19::ServerReply reply;
				reply.set_err_code(nitro_covid19::ServerReply::FAILURE);
				reply.SerializeToString(&out_msg);
			}
			// send reply back to client
			send(out_msg);
			// delete allocated memory from parsing
			delete msg_builder;
		}
	}
}


int main(int argc, char *argv[]) {
	GOOGLE_PROTOBUF_VERIFY_VERSION;

	server_c server;
	server.run();

	google::protobuf::ShutdownProtobufLibrary();
	return 0;
}
