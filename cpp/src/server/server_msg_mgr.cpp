#include "server_msg_mgr.hpp"

#include <boost/algorithm/string.hpp>


/*
 * Build reply message for start_session command using protobuf.
 *
 * Parameters:
 *  @reply_str(Out) - Protobuf start_session reply message serialized as string.
 *
 * return value - true in case serialization succeeded, false otherwise.
 *
 * Give indication if session is already exist from the same country.
 */
bool start_session_builder_c::build_msg(std::string *reply_str) {
	nitro_covid19::ServerReply reply;

	if(std::find(m_active_sessions->begin(), m_active_sessions->end(), m_country) != m_active_sessions->end()) {
		reply.set_err_code(nitro_covid19::ServerReply::SESSION_ALREADY_EXIST);
	} else {
		m_active_sessions->push_back(m_country);
		reply.set_err_code(nitro_covid19::ServerReply::SUCCESS);
	}

	return reply.SerializeToString(reply_str);
}


/*
 * Build reply message for update_cases command using protobuf.
 *
 * Parameters:
 *  @reply_str(Out) - Protobuf update_cases reply message serialized as string.
 *
 * return value - true in case serialization succeeded, false otherwise.
 */
bool update_cases_builder_c::build_msg(std::string *reply_str) {
	nitro_covid19::ServerReply reply;
	std::fstream db_file_in;
	std::fstream db_file_out;
	std::string line;
	// used to gather data from parsed lines
	std::vector<std::string> line_data;
	// used to gather data for writing new lines
	std::vector<std::string> country_data;

	// setting default to success
	reply.set_err_code(nitro_covid19::ServerReply::SUCCESS);

	db_file_in.open(m_db_file_name, std::ios::in);
	if(!db_file_in.is_open()) {
		reply.set_err_code(nitro_covid19::ServerReply::FAILURE);
	} else {
		// indicate if country exist in DB
		bool is_found = false;
		// iterate file's lines and remove country if exist
		while(std::getline(db_file_in, line)) {
			boost::trim(line);
			boost::split(line_data, line, [](char c){return c == ' ';});
			// check if country exist
			if(m_country.compare(line_data[0]) == 0) {
				is_found = true;
				continue;
			} else {
				country_data.push_back(line);
			}
		}
		db_file_in.close();
		if(is_found) {
			db_file_out.open(m_db_file_name, std::ios::out | std::ios::trunc);
			if(!db_file_out.is_open()) {
				reply.set_err_code(nitro_covid19::ServerReply::FAILURE);
			} else {
				// write all countries without current
				for(std::vector<std::string>::iterator it = country_data.begin(); it != country_data.end(); ++it) {
					db_file_out << *it << std::endl;
				}
				// add new deaths and cases
				db_file_out <<\
						m_country + " " + std::to_string(m_dead_people) + " " +\
						std::to_string(m_sick_people) << std::endl;
				db_file_out.close();
			}
		} else {
			db_file_out.open(m_db_file_name, std::ios::out | std::ios::app);
			db_file_out <<\
					m_country + " " + std::to_string(m_dead_people) + " " +\
					std::to_string(m_sick_people) << std::endl;
			db_file_out.close();
		}
	}

	return reply.SerializeToString(reply_str);
}


/*
 * Build reply message for get_cases command using protobuf.
 *
 * Parameters:
 *  @reply_str(Out) - Protobuf get_cases reply message serialized as string.
 *
 * return value - true in case serialization succeeded, false otherwise.
 *
 * The number of sick people is returned with reply message,
 * if no country is given as parameter, number of all sick people in the world
 * will be returned.
 */
bool get_cases_builder_c::build_msg(std::string *reply_str) {
	nitro_covid19::ServerReply reply;
	std::fstream db_file;
	std::string line;
	// used to gather data from parsed lines
	std::vector<std::string> line_data;
	// used to gather data for writing new lines
	std::vector<std::string> country_data;

	// setting default to success
	reply.set_err_code(nitro_covid19::ServerReply::SUCCESS);

	db_file.open(m_db_file_name, std::ios::in);
	if(!db_file.is_open()) {
		reply.set_err_code(nitro_covid19::ServerReply::FAILURE);
	} else {
		bool is_found = false;
		int sum_of_sick_people = 0;
		// iterate file's lines
		while(std::getline(db_file, line)) {
			boost::trim(line);
			boost::split(line_data, line, [](char c){return c == ' ';});
			// check if country exist
			if(m_country_to_query.empty()){
				sum_of_sick_people += std::stoi(line_data[2]);
			} else if(m_country_to_query.compare(line_data[0]) == 0) {
					reply.set_sick_people(std::stoi(line_data[2]));
					is_found = true;
					break;
			}
		}
		if(m_country_to_query.empty()) {
			reply.set_sick_people(sum_of_sick_people);
		} else if(!is_found) {
				reply.set_err_code(nitro_covid19::ServerReply::COUNTRY_NOT_EXIST);
		}
		db_file.close();
	}

	return reply.SerializeToString(reply_str);
}


/*
 * Build reply message for end_session command using protobuf.
 *
 * Parameters:
 *  @reply_str(Out) - Protobuf end_session reply message serialized as string.
 *
 * return value - true in case serialization succeeded, false otherwise.
 *
 * Before sending reply message the country is removed from active sessions.
 */
bool end_session_builder_c::build_msg(std::string *reply_str) {
	nitro_covid19::ServerReply reply;

	m_active_sessions->erase(
			 std::remove(m_active_sessions->begin(), m_active_sessions->end(), m_country),
			 m_active_sessions->end());
	 reply.set_err_code(nitro_covid19::ServerReply::SUCCESS);

	return reply.SerializeToString(reply_str);
}
