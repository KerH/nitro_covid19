#ifndef CPP_SRC_SERVER_MSG_MGR_HPP_
#define CPP_SRC_SERVER_MSG_MGR_HPP_

#include <iostream>
#include <fstream>
#include <string>

#include "nitro_covid19.pb.h"


/*
 * Server reply message builder abstract class.
 *
 * Define a must have method of building a message
 * according to the relevant command.
 */
class server_msg_builder_c
{
public:
	virtual bool build_msg(std::string *reply_str) = 0;
	virtual ~server_msg_builder_c(void) {};
protected:
	server_msg_builder_c(void){};
};


/*
 * Reply builder class for start_session command.
 */
class start_session_builder_c : public server_msg_builder_c
{
public:
	start_session_builder_c(std::string country, std::vector<std::string> *active_sessions) :
		server_msg_builder_c(), m_country(country), m_active_sessions(active_sessions) {};
	bool build_msg(std::string *reply_str);
private:
	std::string m_country;
	std::vector<std::string> *m_active_sessions;
};


/*
 * Reply builder class for update_cases command.
 */
class update_cases_builder_c : public server_msg_builder_c
{
public:
	update_cases_builder_c(
			std::string country, uint32_t sick_people, uint32_t dead_people,
			const std::string &db_file_name) : server_msg_builder_c(),
			m_country(country), m_sick_people(sick_people), m_dead_people(dead_people),
			m_db_file_name(db_file_name) {};
	bool build_msg(std::string *reply_str);
private:
	std::string m_country;
	uint32_t m_sick_people;
	uint32_t m_dead_people;
	std::string m_db_file_name;
};


/*
 * Reply builder class for get_cases command.
 */
class get_cases_builder_c : public server_msg_builder_c
{
public:
	get_cases_builder_c(std::string country, const std::string &db_file_name) :
		server_msg_builder_c(), m_country_to_query(country), m_db_file_name(db_file_name) {};
	bool build_msg(std::string *reply_str);
private:
	std::string m_country_to_query;
	std::string m_db_file_name;
};


/*
 * Reply builder class for end_session command.
 */
class end_session_builder_c : public server_msg_builder_c
{
public:
	end_session_builder_c(std::string country, std::vector<std::string> *active_sessions) :
		server_msg_builder_c(), m_country(country), m_active_sessions(active_sessions) {};
	bool build_msg(std::string *reply_str);
private:
	std::string m_country;
	std::vector<std::string> *m_active_sessions;
};


#endif /* CPP_SRC_SERVER_MSG_MGR_HPP_ */
