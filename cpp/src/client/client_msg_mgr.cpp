#include "client_msg_mgr.hpp"

#include <boost/algorithm/string.hpp>


/*
 * Build message for start_session command using protobuf.
 *
 * Parameters:
 *  @cmd_str(Out) - Protobuf start_session message serialized as string.
 *  @country(In)  - Client country.
 *
 * return value - true in case serialization succeeded, false otherwise.
 */
bool start_session_c::build_msg(std::string *cmd_str, std::string &country) {
	nitro_covid19::ClientCommand cmd;

	cmd.set_message_type(nitro_covid19::ClientCommand::START_SESSION);
	cmd.set_country(country);

	return cmd.SerializeToString(cmd_str);
}


/*
 * Parse reply message from server for start_session command.
 *
 * Parameters:
 *  @reply_str(In) - Protobuf reply message from string serialized as string.
 *  @cli(In) - Client CLI, used for interacting with client.
 *
 * return value - true in case of server success, false otherwise.
 */
bool start_session_c::parse_msg(std::string *reply_str, client_cli_c *cli) {
	nitro_covid19::ServerReply reply;
	if(!reply.ParseFromString(*reply_str)) {
		return false;
	}
	// return true in case success returned from server and false otherwise
	return reply.err_code() == nitro_covid19::ServerReply::SUCCESS;
}


/*
 * Build message for update_cases command using protobuf.
 *
 * Parameters:
 *  @cmd_str(Out) - Protobuf update_cases message serialized as string.
 *  @country(In)  - Client country.
 *
 * return value - true in case serialization succeeded, false otherwise.
 */
bool update_cases_c::build_msg(std::string *cmd_str, std::string &country) {
	nitro_covid19::ClientCommand cmd;
	nitro_covid19::ClientCommand::CmdArgs *args = cmd.add_args();


	cmd.set_message_type(nitro_covid19::ClientCommand::UPDATE_CASES);
	cmd.set_country(country);
	args->set_dead_people(m_dead_people);
	args->set_sick_people(m_sick_people);

	return cmd.SerializeToString(cmd_str);
}


/*
 * Parse reply message from server for update_cases command.
 *
 * Parameters:
 *  @reply_str(In) - Protobuf reply message from string serialized as string.
 *  @cli(In) - Client CLI, used for interacting with client.
 *
 * return value - true in case of server success, false otherwise.
 */
bool update_cases_c::parse_msg(std::string *reply_str, client_cli_c *cli) {
	nitro_covid19::ServerReply reply;
	if(!reply.ParseFromString(*reply_str)) {
		return false;
	}
	// return true in case success returned from server and false otherwise
	return reply.err_code() == nitro_covid19::ServerReply::SUCCESS;
}


/*
 * Build message for get_cases command using protobuf.
 *
 * Parameters:
 *  @cmd_str(Out) - Protobuf get_cases message serialized as string.
 *  @country(In)  - Client country.
 *
 * return value - true in case serialization succeeded, false otherwise.
 */
bool get_cases_c::build_msg(std::string *cmd_str, std::string &country) {
	nitro_covid19::ClientCommand cmd;
	nitro_covid19::ClientCommand::CmdArgs *args = cmd.add_args();

	cmd.set_message_type(nitro_covid19::ClientCommand::GET_CASES);
	cmd.set_country(country);
	args->set_country(m_country_to_query);

	return cmd.SerializeToString(cmd_str);
}


/*
 * Parse reply message from server for get_cases command.
 *
 * Parameters:
 *  @reply_str(In) - Protobuf reply message from string serialized as string.
 *  @cli(In) - Client CLI, used for interacting with client.
 *
 * return value - true in case of server success, false otherwise.
 */
bool get_cases_c::parse_msg(std::string *reply_str, client_cli_c *cli) {
	nitro_covid19::ServerReply reply;
	if(!reply.ParseFromString(*reply_str)) {
		return false;
	}
	if(reply.err_code() == nitro_covid19::ServerReply::SUCCESS) {
		// get number of sick people from msg
		cli->print_sick_people(reply.sick_people());
	} else if(reply.err_code() == nitro_covid19::ServerReply::COUNTRY_NOT_EXIST) {
		cli->print_no_such_country();
	} else {
		return false;
	}
	return true;
}


/*
 * Build message for end_session command using protobuf.
 *
 * Parameters:
 *  @cmd_str(Out) - Protobuf end_session message serialized as string.
 *  @country(In)  - Client country.
 *
 * return value - true in case serialization succeeded, false otherwise.
 */
bool end_session_c::build_msg(std::string *cmd_str, std::string &country) {
	nitro_covid19::ClientCommand cmd;

	// setting msg type to start_session
	cmd.set_message_type(nitro_covid19::ClientCommand::END_SESSION);

	// setting client's country
	cmd.set_country(country);

	return cmd.SerializeToString(cmd_str);
}


/*
 * Parse reply message from server for end_session command.
 *
 * Parameters:
 *  @reply_str(In) - Protobuf reply message from string serialized as string.
 *  @cli(In) - Client CLI, used for interacting with client.
 *
 * return value - true in case of server success, false otherwise.
 */
bool end_session_c::parse_msg(std::string *reply_str, client_cli_c *cli) {
	nitro_covid19::ServerReply reply;
	if(!reply.ParseFromString(*reply_str)) {
		return false;
	}
	// return true in case success returned from server and false otherwise
	return reply.err_code() == nitro_covid19::ServerReply::SUCCESS;
}
