#include <iostream>
#include <string>
#include <vector>

#include "client_cli.hpp"

#include <boost/algorithm/string.hpp>


/*
 * Print welcome message for client.
 *
 * Parameters:
 *  @country(Out) - Client country is updated to this variable.
 */
void client_cli_c::print_welcome_msg(std::string *country)
{
	std::cout << "Welcome to corona virus information system." << std::endl;
	std::cout << "Please authenticate with country name:" << std::endl;
	std::getline(std::cin, *country);
	std::cout << "Welcome " << *country << ", Hope things are getting better." << std::endl << std::endl;
}


/*
 * Print menu with command options for client.
 */
void client_cli_c::print_menu(void) const
{
	std::cout << "You can choose one of the following:" << std::endl;
	std::cout << "1) update_cases [#deaths] [#cases] - for updating data in server" << std::endl;
	std::cout << "2) get_cases [country] - for getting number of cases from a specific country." << std::endl;
	std::cout << "                         in case argument is empty you will get data on all countries." << std::endl;
	std::cout << "3) exit - for closing application" << std::endl;
	std::cout << "4) [h|help] - for showing this information again" << std::endl;
}


/*
 * Print session error for client.
 *
 * This print is called when client is getting
 * errors while receiving or parsing a
 * `start_session` message reply from server.
 */
void client_cli_c::print_session_err(void) const
{
	std::cout << "Error: Can't create session with server" << std::endl;
}


/*
 * Print server error for client.
 *
 * This print is called when client is getting
 * errors while receiving or parsing a message from server.
 */
void client_cli_c::print_server_err(void) const
{
	std::cout << "Error: Server had problems with latest command\nPlease try again" << std::endl;
}


/*
 * Print client error for client.
 *
 * This print is called when building a message from
 * client fails.
 */
void client_cli_c::print_client_err(void) const
{
	std::cout << "Unexpected Error occured, try again" << std::endl;
}


/*
 * Print sick people number to client.
 */
void client_cli_c::print_sick_people(uint32_t sick_people) const
{
	std::cout << "Number of sick people: " + std::to_string(sick_people) << std::endl;
}


/*
 * Print indication that the country does not exist.
 */
void client_cli_c::print_no_such_country(void) const
{
	std::cout << "Given country does not exist in server DB yet" << std::endl;
}


/*
 * Get command from client until it's valid.
 * Prints a reminder for command option every MAX_ERR_TRIES tries.
 *
 * return value - the valid command.
 */
std::string client_cli_c::insert_cmd(void)
{
	int timeout = MAX_ERR_TRIES;
	std::string cmd;
	std::cout << "Please insert your choice:" << std::endl;
	std::getline(std::cin, cmd);
	while(!is_cmd_valid(cmd)) {
		timeout--;
		if(timeout == 0) {
			print_menu();
			timeout = MAX_ERR_TRIES;
		}
		std::cout << "Please insert your choice again:" << std::endl;
		std::getline(std::cin, cmd);
	}
	return cmd;
}


/*
 * Check if given command is valid.
 *
 * Parameters:
 *  @cmd(In) - string representation of command as received from console.
 *
 * return value - True in case command is valid, false otherwise.
 *
 * Because we are passing the command for client to handle it
 * we return false alse for 'h' or 'help' command, since it's
 * got no value for client user to handle(i.e - send it to server).
 */
bool client_cli_c::is_cmd_valid(std::string &cmd)
{
	std::vector<std::string> cmd_args;

	// trim whitespaces in given command
	boost::trim(cmd);
	boost::split(cmd_args, cmd, [](char c){return c == ' ';});

	if(cmd_args[0].compare("update_cases") == 0) {
		// update_cases command received
		char* arg_end;
		if(cmd_args.size() != 3 || std::strtol(cmd_args[1].c_str(), &arg_end, 10) < 0
				|| std::strtol(cmd_args[2].c_str(), &arg_end, 10) < 0) {
			std::cout << "Please use update_cases [#deaths] [#cases]" << std::endl;
			return false;
		}
	} else if(cmd_args[0].compare("get_cases") == 0) {
		// get_cases command received
		if(cmd_args.size() != 1 && cmd_args.size() != 2) {
			std::cout << "Please use get_cases [country]" << std::endl;
			return false;
		}
	} else if(cmd_args[0].compare("exit") == 0) {
		// exit command received
		return true;
	} else if(cmd.compare("h") == 0 || cmd.compare("help") == 0) {
		// help command received
		print_menu();
		return false;
	} else {
		std::cout << "Invalid command!" << std::endl;
		return false;
	}
	return true;
}
