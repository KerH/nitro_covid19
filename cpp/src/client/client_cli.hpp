#ifndef SRC_UTILS_CLIENT_CLI_C_HPP_
#define SRC_UTILS_CLIENT_CLI_C_HPP_


#define MAX_ERR_TRIES 5


/*
 * Client command line interface class.
 *
 * Responsible for getting data from client
 * (e.g - client country, commands), and for printing
 * indications on command status.
 */
class client_cli_c
{
public:
	void print_welcome_msg(std::string *country);
	void print_menu(void) const;
	void print_session_err(void) const;
	void print_server_err(void) const;
	void print_client_err(void) const;
	void print_sick_people(uint32_t sick_people) const;
	void print_no_such_country(void) const;
	std::string insert_cmd(void);

private:
	bool is_cmd_valid(std::string &cmd);
};


#endif /* SRC_UTILS_CLIENT_CLI_C_HPP_ */
