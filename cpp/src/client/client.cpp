#include <nitro_covid19/client.hpp>

#include <boost/algorithm/string.hpp>


#define RECV_TIMEOUT 1000


/*
 * Client constructor.
 */
client_c::client_c(void) {
	// create context and socket
	m_ctx = new zmq::context_t(1);
	m_sock = new zmq::socket_t(*m_ctx, ZMQ_REQ);

	// create client cli
	m_cli = client_cli_c();
	m_cli.print_welcome_msg(&m_country);

	//  connect socket to server
	(*m_sock).connect(IPC_PATH);
}


/*
 * Client destructor.
 */
client_c::~client_c(void) {
	// closing socket
	(*m_sock).close();
	delete m_sock;

	// context closed and deleted
	delete m_ctx;
}


/*
 * Send message to server over zmq socket.
 *
 * Parameters:
 *  @cmd(In) - String serialization of protobuf converted command from client.
 *
 * return value - true in case send succeeded, false otherwise.
 */
bool client_c::send(const std::string &cmd) {
	zmq::message_t msg(cmd.size());
    std::memcpy(msg.data(), cmd.data(), cmd.size());
    bool rc = (*m_sock).send(msg);
    return (rc);
}


/*
 * Poll on socket with timeout until a reply message received from server.
 *
 * Parameters:
 *  @items(In)      - Array of sockets to poll for events.
 *  @reply_str(Out) - Protobuf message replied from server serialized as string.
 *
 * return value - true after receiving a message, false in case of polling error.
 */
bool client_c::poll_recv(zmq::pollitem_t items[], std::string *reply_str) {
	zmq::message_t reply;
	while(1) {
		int poll_rc = zmq::poll(&items[0], 1, RECV_TIMEOUT);
		if(poll_rc < 0) {
			std::cout << "Error occured while polling for reply from server:" +\
					std::to_string(poll_rc) << std::endl;
			return false;
		}
		if (items[0].revents & ZMQ_POLLIN) {
			// Get reply from server
			(*m_sock).recv(&reply, 0);
			*reply_str = std::string(static_cast<char*>(reply.data()), reply.size());
			break;
		}
	}
	return true;
}


/*
 * Handle client command.
 *
 * Parameters:
 *  @cmd_str(In)          - Client command as client inserted in CLI.
 *  @items(In)            - Array of sockets to poll for events.
 *  @is_start_session(In) - flag to indicate if it's start_session message.
 *
 * return value - true except in the next cases:
 *  			   1) end_session command received and parsed correctly at server side
 *  			   2) start_session failed
 */
bool client_c::handle_cmd(
		std::string cmd_str, zmq::pollitem_t items [], bool is_start_session) {
	// client command
	std::vector<std::string> cmd_args;
	client_cmd_c* cmd;
	// reply message from server variable
	std::string reply_str;

	// flag for indicating exit message
	bool should_continue = true;

	// extract command arguments
	boost::trim(cmd_str);
	boost::split(cmd_args, cmd_str, [](char c){return c == ' ';});

	if(is_start_session){
		cmd = new start_session_c;
	} else if(cmd_args[0].compare("update_cases") == 0) {
		char* arg_end;
		uint32_t dead_people = std::strtol(cmd_args[1].c_str(), &arg_end, 10);
		uint32_t sick_people = std::strtol(cmd_args[2].c_str(), &arg_end, 10);
		cmd = new update_cases_c(dead_people, sick_people);
	} else if(cmd_args[0].compare("get_cases") == 0) {
		std::string country_to_query("");
		if(cmd_args.size() == 2){
			country_to_query = cmd_args[1];
		}
		cmd = new get_cases_c(country_to_query);
	} else if(cmd_args[0].compare("exit") == 0) {
		cmd = new end_session_c;
		should_continue = false;
	}
	// build & send message, get & parse answer
	if(cmd->build_msg(&cmd_str, m_country)) {
		send(cmd_str);
		if(!poll_recv(items, &reply_str) || !cmd->parse_msg(&reply_str, &m_cli)) {
			if(is_start_session) {
				m_cli.print_session_err();
				should_continue = false;
			} else {
				m_cli.print_server_err();
				should_continue = true;
			}
		}
	} else {
		m_cli.print_client_err();
	}
	delete cmd;
	return should_continue;
}


/*
 * Main client function.
 */
void client_c::run(void) {
	//  initialize poll set
	zmq::pollitem_t items [] = {
		{static_cast<void*>(*m_sock), 0, ZMQ_POLLIN, 0}
	};

	// start session with server
	if(!handle_cmd("", items, true)) {
		return;
	}

	// printing menu to user
	m_cli.print_menu();
	// handle command from user
	while(handle_cmd(m_cli.insert_cmd(), items)) {
		// this stops when exit command received or a session can't be created
	}
}


int main(int argc, char *argv[]){
	GOOGLE_PROTOBUF_VERIFY_VERSION;
	client_c client;
	client.run();

	google::protobuf::ShutdownProtobufLibrary();
	return 0;
}
