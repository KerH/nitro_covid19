# create list of client source files
list(APPEND CLIENT_SRCS client_cli.cpp client_msg_mgr.cpp client.cpp)

# protobuf src's will be created at compile time
set_property(SOURCE ${PROTO_SRCS} PROPERTY GENERATED TRUE)

# create client exe
add_executable(client_app ${CLIENT_SRCS} ${PROTO_SRCS})

# depend exe on protobuf generated files
add_dependencies(client_app protobuf_generated_files)

# add include files
target_include_directories(client_app
	PRIVATE
	${PROJECT_SOURCE_DIR}/include
	${CMAKE_CURRENT_BINARY_DIR}
	${Protobuf_INCLUDE_DIRS}
	${PROTOBUF_GENERATED_FILES_DIR}
	${cppzmq_INCLUDE_DIRS}
)

# link boost & cppzmq & protobuf to client
target_link_libraries(client_app
	${Boost_LIBRARIES}
	${ZeroMQ_LIBRARY}
	${Protobuf_LIBRARIES})
