#ifndef CPP_SRC_UTILS_CLIENT_MGS_MGR_HPP_
#define CPP_SRC_UTILS_CLIENT_MSG_MGR_HPP_

#include <string>

#include "nitro_covid19.pb.h"

#include "client_cli.hpp"


/*
 * Client command abstract class.
 *
 * All client commands have representation by protobuf messages(can look at .proto file),
 * supported commands are:
 * 	- start_session
 * 	- update_cases
 *	- get_cases
 *	- end_session
 *
 *	Each of the above should have it's own building and parsing,
 *	this class demands it, and the above commands inherits from this class.
 */
class client_cmd_c
{
public:
	virtual bool build_msg(std::string *cmd_str, std::string &country) = 0;
	virtual bool parse_msg(std::string *reply_str, client_cli_c *cli) = 0;
	virtual ~client_cmd_c(void){};
protected:
	client_cmd_c(void){};
};


/*
 * Start session command class.
 *
 * Responsible for creating start_session command
 * and parsing matching reply by associated protobuf encoding.
 */
class start_session_c : public client_cmd_c
{
public:
	bool build_msg(std::string *cmd_str, std::string &country);
	bool parse_msg(std::string *reply_str, client_cli_c *cli);
};


/*
 * Update cases command class.
 *
 * Responsible for creating update_cases command
 * and parsing matching reply by associated protobuf encoding.
 */
class update_cases_c : public client_cmd_c
{
public:
	update_cases_c(uint32_t dead_people, uint32_t sick_people) :
		client_cmd_c(), m_dead_people(dead_people), m_sick_people(sick_people){};
	bool build_msg(std::string *cmd_str, std::string &country);
	bool parse_msg(std::string *reply_str, client_cli_c *cli);

private:
	uint32_t m_dead_people;
	uint32_t m_sick_people;
};


/*
 * Get cases command class.
 *
 * Responsible for creating get_cases command
 * and parsing matching reply by associated protobuf encoding.
 */
class get_cases_c : public client_cmd_c
{
public:
	explicit get_cases_c(std::string country_to_query) :
		client_cmd_c(), m_country_to_query(country_to_query) {};
	bool build_msg(std::string *cmd_str, std::string &country);
	bool parse_msg(std::string *reply_str, client_cli_c *cli);
private:
	std::string m_country_to_query;
};


/*
 * End session command class.
 *
 * Responsible for creating end_session command
 * and parsing matching reply by associated protobuf encoding.
 */
class end_session_c : public client_cmd_c
{
public:
	bool build_msg(std::string *cmd_str, std::string &country);
	bool parse_msg(std::string *reply_str, client_cli_c *cli);
};


#endif /* CPP_SRC_UTILS_CLIENT_MSG_MGR_HPP_ */
